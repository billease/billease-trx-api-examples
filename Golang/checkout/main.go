package main

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {

	url := "https://trx-test.billease.ph/be-transactions-api/trx/checkout"

	payload := strings.NewReader("{\n  \"shop_id\": \"D5103E13-3EEC-44C3-A1B3-AE02592C4C43\",\n  \"amount\": 20000,\n  \"currency\": \"PHP\",\n  \"merchant_id\": \"73CB6A3A-67AB-43E8-9267-E04ABA1CA77E\",\n  \"checkout_type\": \"standard\",\n  \"items\": [\n    {\n      \"code\": \"SKU123e4567\",\n      \"item\": \"iPhone Xs\",\n      \"price\": 10000,\n      \"quantity\": 1,\n      \"currency\": \"PHP\",\n      \"url_item\": \"https://www.lazada.com.ph/products/apple-iphone-xs-i262561730-s364449221.html?spm=a2o4l.searchlist.list.6.2d46599fbfVZWH&search=1\",\n      \"url_img\": \"https://upload.wikimedia.org/wikipedia/commons/3/3b/IPhone_5s_top.jpg\",\n      \"category\": \"mobile\",\n      \"seller_code\": \"ALPHABET123\",\n      \"item_type\": \"item\"\n    },\n    {\n      \"code\": \"SKU123e4568\",\n      \"item\": \"MacBook Pro\",\n      \"price\": 10000,\n      \"quantity\": 1,\n      \"currency\": \"PHP\",\n      \"url_item\": \"https://www.lazada.com.ph/products/macbook-pro-133-matte-protective-case-gold-i100057364-s100071533.html?spm=a2o4l.searchlistbrand.list.17.4fd7afdfABECD6&search=1\",\n      \"url_img\": \"https://upload.wikimedia.org/wikipedia/commons/1/1d/MacBook_Pro%2C_Late-2008.jpg\",\n      \"category\": \"mobile\",\n      \"seller_code\": \"ALPHABET123\",\n      \"item_type\": \"item\"\n    }\n  ],\n  \"sellers\": [\n    {\n      \"code\": \"ALPHABET123\",\n      \"seller_name\": \"Alphabet inc.\",\n      \"url\": \"http://example.com\",\n      \"email\": \"info@example.com\",\n      \"phone\": \"+639054196316\",\n      \"country\": \"PH\",\n      \"province\": \"NRC\",\n      \"city\": \"Manila\",\n      \"barangay\": \"Makati\",\n      \"street\": \"Paseo de Roxas 104\",\n      \"address\": \"Address in one line\"\n    }\n  ],\n  \"customer\": {\n    \"full_name\": \"Vitalii Sharavara\",\n    \"email\": \"sharavara@example.com\",\n    \"phone\": \"+639054194316\",\n    \"adr_billing\": {\n      \"addr_type\": \"billing\",\n      \"country\": \"PH\",\n      \"province\": \"NCR\",\n      \"city\": \"Manila\",\n      \"barangay\": \"Makati\",\n      \"street\": \"Paseo de Roxas 104\",\n      \"address\": \"Address in one line\"\n    },\n    \"adr_shipping\": {\n      \"addr_type\": \"shipping\",\n      \"country\": \"PH\",\n      \"province\": \"NCR\",\n      \"city\": \"Manila\",\n      \"barangay\": \"Makati\",\n      \"street\": \"Paseo de Roxas 104\",\n      \"address\": \"Address in one line\"\n    }\n  },\n  \"callbackapi_url\": \"https://example.com/api/update\",\n  \"url_redirect\": \"https://example.com/xxx.html\",\n  \"order_id\": \"ORDER-23\"\n}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJiaWxsZWFzZS1qd3QiLCJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkFwcGxlIGFuZCBjby4iLCJpYXQiOjE1MTYyMzkwMjIsImNvZGUiOiIxMjNlNDU2Ny1lODliLTEyZDMtYTQ1Ni00MjY2NTU0NDAwMDAiLCJ0b2tlbmlkIjoiZHNmYmFocTIzMjUzd2VyaGZhbzY4cXBic2RmODM0OTJnZWhqciIsImtleSI6InVpdHd5ZXJiZGpzYWYzNDk4MmtqaGdhc2RmMDI5MWFmazI5MCJ9.uV24Chf32ECnsGpUNqU_PMNjVexjmF-K59c7YeIS3mE")

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
