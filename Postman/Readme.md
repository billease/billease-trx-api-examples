# How to use Postman Collection

## Postman

Postman is a collaboration platform for API development

Here you can download the application: [https://www.postman.com/](https://www.postman.com/)

## Create Environment

Our example uses environment variables. You have to create staging environment like on this example: 

![image-20200414132155574](assets/image-20200414132155574.png)

Environment Variables:

![image-20200414132255034](assets/image-20200414132255034.png)

`token` - your token for staging environemnt

`merchant_code` - merchant code

`shop_code` - shop code

`transaction_code` - this is transaction ID. You will get it from the response of `Create transaction` method



## Example

![image-20200414135205162](assets/image-20200414135205162.png)

In current example we created a transaction with a few items. You have to setup variable `transaction_code` from this responce:



![image-20200414135359327](assets/image-20200414135359327.png)



After that you can update the transaction (or check status):



![image-20200414135524335](assets/image-20200414135524335.png)

## Callback

Also you can test your callbak API:

![image-20200414135705090](assets/image-20200414135705090.png)



Here is a ping of callback API:

![image-20200414140030747](assets/image-20200414140030747.png)

You have to update Authorization depend's on your implementation. We support three types of  authorization:

- Berear Token
- API Key
- Basith authentication

Also, don't forget update the endpoint