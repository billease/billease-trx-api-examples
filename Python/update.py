import http.client
import json
import ssl

context = ssl._create_unverified_context()
host = 'trx-test.billease.ph'
conn = http.client.HTTPSConnection(host, context=context)

trx_id = '2c852211-75ed-4db3-ae95-81d7c054d7aa'

url = f'/be-transactions-api/trx/{trx_id}/update'

headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJiaWxsZWFzZS1qd3QiLCJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkFwcGxlIGFuZCBjby4iLCJpYXQiOjE1MTYyMzkwMjIsImNvZGUiOiIxMjNlNDU2Ny1lODliLTEyZDMtYTQ1Ni00MjY2NTU0NDAwMDAiLCJ0b2tlbmlkIjoiZHNmYmFocTIzMjUzd2VyaGZhbzY4cXBic2RmODM0OTJnZWhqciIsImtleSI6InVpdHd5ZXJiZGpzYWYzNDk4MmtqaGdhc2RmMDI5MWFmazI5MCJ9.uV24Chf32ECnsGpUNqU_PMNjVexjmF-K59c7YeIS3mE'
    }

payload = {
  'order_id': 'ORDER-23',
  'status': 'OK'
}

conn.request('POST', url, body=json.dumps(payload), headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode('utf-8'))
